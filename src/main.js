// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '~/layouts/Default.vue';
import ArticleComponent from '~/components/Article.vue';
import BannerComponent from '~/components/Banner.vue';

export default function (Vue, { router, head, isClient }) {
  Vue.component('Layout', DefaultLayout),
  Vue.component('Article', ArticleComponent),
  Vue.component('Banner', BannerComponent)
}
