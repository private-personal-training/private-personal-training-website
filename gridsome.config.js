// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Private Personal Training',
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'static/testimonial/**/*.md',
        typeName: 'Testimonial',
        remark: {}
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'static/content/**/*.md',
        typeName: 'Content',
        remark: {}
      }
    }
  ],
  transformers: {
    remark: {
      // global remark options
    }
  }
}
