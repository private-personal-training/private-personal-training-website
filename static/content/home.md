---
id: home
title: Home
banner: /uploads/lobby-1.jpeg
---

# Have the Gym to Yourself!

Private Personal Training & Sacred Earth Yoga is a fitness studio in Shelburne Vermont located on the bottom floor of the Creamery Building on Route 7 in the the heart of the village! 

Here we provide a private and safe environment for folks to establish and maintain personal fitness goals! Here we provide guidance and supervison for strength & cardiovascular presurgical conditioning and postsurgical rehabilitation! Our cardio studio is available by appointment for private use and yoga classes may be attended in person or via Zoom!