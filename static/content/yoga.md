---
id: yoga
title: Yoga
banner: /uploads/yoga-2.jpeg
---
# Yoga

Here at Sacred Earth Yoga we practice an old school, traditional Ashtanga yoga practice (1/2 Primary Series). All classes may be attended in person or on Zoom! All classes are suitable for all ages and levels of ability! Complimentary first class orientation is available by appointment!

### Class Schedule:

* MWF 10:00am
* M TH 6:00am
* Sun 8:30am

First class is complimentary, there after, donations would be happily accepted (suggested donation $100 per 10 sessions)! Yoga classes are included with personal training!